先自我介绍一下，本人12年一本大学，毕业就职业国企，干了一年多以后深感无趣，于是辞职自学java，从月薪6000一步步到阳光、国美等大厂，因本人就从泥沼中走来。所以深知大多数初中级Java工程师，想要提升技能，往往是自己摸索成长或者是报班学习，但对于培训机构动则近万的学费，着实压力不小。自己不成体系的自学效果低效又漫长，而且极易碰到天花板技术停滞不前！

 **因此收集整理了一份《Java开发全套学习资料》送给大家，初衷也很简单，就是希望能够帮助到想自学提升又不知道该从何学起的朋友，同时减轻大家的负担。** 
    
 _**有需要的可以加我**_   :point_down:  :point_down: 

![输入图片说明](file/v.png)

![输入图片说明](file/1.png)
![输入图片说明](file/2.webp)

 **由于文件比较大，这里只是将部分目录截图出来，每个节点里面都包含大厂面经、学习笔记、源码讲义、实战项目、讲解视频！** 